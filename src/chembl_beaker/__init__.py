__author__ = 'mnowotka'

import rdkit

try:
    __version__ = '1.6.0'
except Exception as e:
    __version__ = 'development'


rdkversion = rdkit.__version__.split(".")
if rdkversion < ["2022", "09", "4"]:
    raise ValueError("need an RDKit version >= 2022.09.4")